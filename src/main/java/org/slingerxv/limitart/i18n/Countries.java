/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package org.slingerxv.limitart.i18n;

/**
 * 国家简写
 * 
 * @author hank
 *
 */
public enum Countries {

	/**
	 * 阿拉伯联合酋长国
	 */
	AE,

	/**
	 * 阿富汗伊期兰共和国
	 */
	AF,

	/**
	 * 阿根廷
	 */
	AR,

	/**
	 * 奥地利
	 */
	AT,

	/**
	 * 澳大利亚
	 */
	AU,

	/**
	 * 巴巴多斯
	 */
	BB,

	/**
	 * 孟加拉国
	 */
	BD,

	/**
	 * 比利时
	 */
	BE,

	/**
	 * 保加利亚
	 */
	BG,

	/**
	 * 玻利维亚
	 */
	BO,

	/**
	 * 巴西
	 */
	BR,

	/**
	 * 巴哈马
	 */
	BS,

	/**
	 * 不丹
	 */
	BT,

	/**
	 * 加拿大
	 */
	CA,

	/**
	 * 瑞士
	 */
	CH,

	/**
	 * 智利
	 */
	CL,

	/**
	 * 中国
	 */
	CN,

	/**
	 * 哥伦比亚
	 */
	CO,

	/**
	 * 哥斯达黎加
	 */
	CR,

	/**
	 * 古巴
	 */
	CU,

	/**
	 * 捷克共和国
	 */
	CZ,

	/**
	 * 德国
	 */
	DE,

	/**
	 * 丹麦
	 */
	DK,

	/**
	 * 阿尔及利亚
	 */
	DZ,

	/**
	 * 埃及
	 */
	EG,

	/**
	 * 西班牙
	 */
	ES,

	/**
	 * 埃塞俄比亚
	 */
	ET,

	/**
	 * 芬兰
	 */
	FI,

	/**
	 * 斐济
	 */
	FJ,

	/**
	 * 法国
	 */
	FR,

	/**
	 * 格陵兰
	 */
	GL,

	/**
	 * 希腊
	 */
	GR,

	/**
	 * 中国香港特别行政区
	 */
	HK,

	/**
	 * 匈牙利
	 */
	HU,

	/**
	 * 印度尼西亚
	 */
	ID,

	/**
	 * 爱尔兰
	 */
	IE,

	/**
	 * 以色列
	 */
	IL,

	/**
	 * 印度
	 */
	IN,

	/**
	 * 伊拉克
	 */
	IQ,

	/**
	 * 伊朗
	 */
	IR,

	/**
	 * 冰岛
	 */
	IS,

	/**
	 * 意大利
	 */
	IT,

	/**
	 * 牙买加
	 */
	JM,

	/**
	 * 约旦
	 */
	JO,

	/**
	 * 日本
	 */
	JP,

	/**
	 * 朝鲜
	 */
	KP,

	/**
	 * 韩国
	 */
	KR,

	/**
	 * 老挝
	 */
	LA,

	/**
	 * 黎巴嫩
	 */
	LB,

	/**
	 * 利比亚
	 */
	LY,

	/**
	 * 缅甸
	 */
	MM,

	/**
	 * 蒙古
	 */
	MN,

	/**
	 * 中国澳门特别行政区
	 */
	MO,

	/**
	 * 墨西哥
	 */
	MX,

	/**
	 * 马来西亚
	 */
	MY,

	/**
	 * 荷兰
	 */
	NL,

	/**
	 * 挪威
	 */
	NO,

	/**
	 * 尼泊尔
	 */
	NP,

	/**
	 * 新西兰
	 */
	NZ,

	/**
	 * 巴拿马
	 */
	PA,

	/**
	 * 秘鲁
	 */
	PE,

	/**
	 * 菲律宾
	 */
	PH,

	/**
	 * 巴基斯坦
	 */
	PK,

	/**
	 * 波兰
	 */
	PL,

	/**
	 * 波多黎各
	 */
	PR,

	/**
	 * 葡萄牙
	 */
	PT,
	/**
	 * 巴拉圭
	 */
	PY,

	/**
	 * 罗马尼亚
	 */
	RO,

	/**
	 * 俄罗斯联邦
	 */
	RU,

	/**
	 * 卢旺达
	 */
	RW,

	/**
	 * 沙特阿拉伯
	 */
	SA,

	/**
	 * 所罗门群岛
	 */
	SB,

	/**
	 * 苏丹
	 */
	SD,

	/**
	 * 瑞典
	 */
	SE,

	/**
	 * 新加坡
	 */
	SG,

	/**
	 * 斯洛伐克共和国
	 */
	SK,

	/**
	 * 索马里
	 */
	SO,

	/**
	 * 叙利亚
	 */
	SY,

	/**
	 * 泰国
	 */
	TH,

	/**
	 * 塔吉克斯坦
	 */
	TJ,

	/**
	 * 土耳其
	 */
	TR,

	/**
	 * 中国台湾
	 */
	TW,

	/**
	 * 乌克兰
	 */
	UA,

	/**
	 * 乌干达
	 */
	UG,

	/**
	 * 英国
	 */
	UK,
	/**
	 * 美国
	 */
	US,

	/**
	 * 乌拉圭
	 */
	UY,

	/**
	 * 乌兹别克斯坦
	 */
	UZ,
	/**
	 * 委内瑞拉
	 */
	VE,

	/**
	 * 越南
	 */
	VN,

	/**
	 * 也门
	 */
	YE,

	/**
	 * 马约特
	 */
	YT,

	/**
	 * 南斯拉夫
	 */
	YU,

	/**
	 * 南非
	 */
	ZA,

	/**
	 * 赞比亚
	 */
	ZM,

	/**
	 * 扎伊尔
	 */
	ZR,

	/**
	 * 津巴布韦
	 */
	ZW;

}
